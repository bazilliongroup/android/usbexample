package com.example.usbexample;

import androidx.appcompat.app.AppCompatActivity;


import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Iterator;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    UsbManager manager;
    private TextView textInfo;
    private UsbDevice device;
    private static final String ACTION_USB_PERMISSION = "com.example.usbexample.USB_PERMISSION";

    UsbDeviceConnection fileDescriptor;
    FileInputStream inputStream;
    FileOutputStream outputStream;

    //create instantiated USB receiver class
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();

            if (ACTION_USB_PERMISSION.equals(action))
            {
                synchronized (this)
                {
                    device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false))
                    {
                        if (device != null)
                        {
                            // call method to set up device communication
                        }
                    }
                    else
                    {
                        Log.d("ERROR", "permission denied for device " + device);
                    }
                }
            }
            if (UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action))
            {

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Button btnCheck;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCheck = findViewById(R.id.CheckUsbBut);
        textInfo = findViewById(R.id.DeviceInfoText);
        btnCheck.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View arg0)
            {
                checkInfoUsbAccessory();
            }
        });

    }

    //used in USB Accessory Mode
    private void checkInfoUsbAccessory()
    {
        String i = "";
        PendingIntent mPermissionIntent;

        //Enumerate accessories to get USB device
        manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String , UsbDevice> deviceList = manager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

        /*
         * This block is required. We first ask for permission to use the device.
         * then setup the filter intent and register the USB receiver class with the intent.
         * Then when asking for information, we use the "manager.requestPermission" with
         * the device and intent.
         */
        // ------------------------------------------------------------------
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        registerReceiver(mUsbReceiver, filter);
        // -------------------------------------------------------------------

        while (deviceIterator.hasNext())
        {
            device = deviceIterator.next();

            //you must request permission explicitly in your application before connecting to the device.
            manager.requestPermission(device, mPermissionIntent);

            i += "\n" + "DeviceID: " + device.getDeviceId() + "\n"
                    + "DeviceName: " + device.getDeviceName() + "\n"
                    + "DeviceClass: " + device.getDeviceClass() + " - "
                    + "DeviceSubClass: " + device.getDeviceSubclass() + "\n"
                    + "VendorID: " + device.getVendorId() + "\n"
                    + "ProductID: " + device.getProductId() + "\n";
        }

        if (i == "") { i = "None Found"; }

        textInfo.setText(i);
    }

    private void openAccessory()
    {
        Log.d("Open Accessory", "openAccessory: " + device);
        fileDescriptor = manager.openDevice(device);
        if (fileDescriptor != null)
        {
//            FileDescriptor fd = fileDescriptor.getFileDescriptor();
//            inputStream = new FileInputStream(fd);
//            outputStream = new FileOutputStream(fd);
//            Thread thread = new Thread(null, this, "DeviceThread");
//            thread.start();
        }
    }
}